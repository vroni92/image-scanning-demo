# image-scanning-demo

Container image static analysis demo using Gitlab CI/CD pipelines.

## Steps

### 1. Create a Gitlab Repo
Log into Gitlab.com (or your self managed Gitlab instance). Click the (+) icon in the top navigation bar and select `New Project/Repository` > `Create Blank Project`. Configure your project as needed.

### 2. Add a Dockerfile
This demo will be using `Dockerfile` to create our Docker container image. You can copy the [Dockerfile](./Dockerfile) in this repo as an example. Put this at the root of your project.

### 3. Add a Gitlab CI template
If not automatically included in your project, add a [.gitlab-ci.yml](./.gitlab-ci.yml) file to the root of your project. Add the following contents to use the [Gitlab SAST template](https://docs.gitlab.com/ee/user/application_security/sast/) for scanning container images.

```yaml
include:
  - template: Security/Container-Scanning.gitlab-ci.yml

build:
  image: docker:latest
  stage: build
  services:
    - docker:dind
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  script:
    - docker info
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker build -t $IMAGE .
    - docker push $IMAGE
```

### 4. Push your code and see the results!
Once you `add`, `commit`, and `push` your code, Gitlab will kick of a CI pipeline. You can view all pipelines by navigating to the left sidebar > `CI/CD` > `Pipelines`. There should be two jobs: `build` (which we defined directly in the [.gitlab-ci.yml](./.gitlab-ci.yml)) and `container_scanning` (which is defined in the included `Security/Container-Scanning.gitlab-ci.yml` template).

The `build` job will log into your project's Container Registry, build the image defined by your [Dockerfile](./Dockerfile), and push that image to your project registry. You can view your project registry by navigating to the left sidebar > `Packages & Registries` > `Container Registry`.

The `container_scanning` job will run a static vulnerability analysis on the container images in your project's Container Registry, and display any vulnerabilities found. Here is an example of the results:

```bash

+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
|   STATUS   |      CVE SEVERITY       |   PACKAGE NAME   | PACKAGE VERSION  |                            CVE DESCRIPTION                             |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2011-3374    |       apt        |      2.2.4       | It was found that apt-key in apt, all versions, do not correctly valid |
|            |                         |                  |                  | ate gpg keys with the master keyring, leading to a potential man-in-th |
|            |                         |                  |                  |                            e-middle attack.                            |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2016-2781    |    coreutils     |      8.32-4      | chroot in GNU coreutils, when used with --userspec, allows local users |
|            |                         |                  |                  |  to escape to the parent session via a crafted TIOCSTI ioctl call, whi |
|            |                         |                  |                  |          ch pushes characters to the terminal's input buffer.          |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2017-18018    |    coreutils     |      8.32-4      | In GNU Coreutils through 8.29, chown-core.c in chown and chgrp does no |
|            |                         |                  |                  | t prevent replacement of a plain file with a symlink during use of the |
|            |                         |                  |                  |  POSIX "-R -L" options, which allows local users to modify the ownersh |
|            |                         |                  |                  |         ip of arbitrary files by leveraging a race condition.          |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2011-3374    |  libapt-pkg6.0   |      2.2.4       | It was found that apt-key in apt, all versions, do not correctly valid |
|            |                         |                  |                  | ate gpg keys with the master keyring, leading to a potential man-in-th |
|            |                         |                  |                  |                            e-middle attack.                            |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved | Critical CVE-2021-33574 |     libc-bin     | 2.31-13+deb11u2  | The mq_notify function in the GNU C Library (aka glibc) versions 2.32  |
|            |                         |                  |                  | and 2.33 has a use-after-free. It may use the notification thread attr |
|            |                         |                  |                  | ibutes object (passed through its struct sigevent parameter) after it  |
|            |                         |                  |                  | has been freed by the caller, leading to a denial of service (applicat |
|            |                         |                  |                  |            ion crash) or possibly unspecified other impact.            |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2010-4756    |     libc-bin     | 2.31-13+deb11u2  | The glob implementation in the GNU C Library (aka glibc or libc6) allo |
|            |                         |                  |                  | ws remote authenticated users to cause a denial of service (CPU and me |
|            |                         |                  |                  | mory consumption) via crafted glob expressions that do not match any p |
|            |                         |                  |                  | athnames, as demonstrated by glob expressions in STAT commands to an F |
|            |                         |                  |                  |        TP daemon, a different vulnerability than CVE-2010-2632.        |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2018-20796    |     libc-bin     | 2.31-13+deb11u2  | In the GNU C Library (aka glibc or libc6) through 2.29, check_dst_limi |
|            |                         |                  |                  | ts_calc_pos_1 in posix/regexec.c has Uncontrolled Recursion, as demons |
|            |                         |                  |                  |            trated by '(\227|)(\\1\\1|t1|\\\2537)+' in grep.            |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |  Low CVE-2019-1010022   |     libc-bin     | 2.31-13+deb11u2  | ** DISPUTED ** GNU Libc current is affected by: Mitigation bypass. The |
|            |                         |                  |                  |  impact is: Attacker may bypass stack guard protection. The component  |
|            |                         |                  |                  | is: nptl. The attack vector is: Exploit stack buffer overflow vulnerab |
|            |                         |                  |                  | ility and use this bypass vulnerability to bypass stack guard. NOTE: U |
|            |                         |                  |                  | pstream comments indicate "this is being treated as a non-security bug |
|            |                         |                  |                  |                          and no real threat."                          |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |  Low CVE-2019-1010023   |     libc-bin     | 2.31-13+deb11u2  | ** DISPUTED ** GNU Libc current is affected by: Re-mapping current loa |
|            |                         |                  |                  | ded library with malicious ELF file. The impact is: In worst case atta |
|            |                         |                  |                  | cker may evaluate privileges. The component is: libld. The attack vect |
|            |                         |                  |                  | or is: Attacker sends 2 ELF files to victim and asks to run ldd on it. |
|            |                         |                  |                  |  ldd execute code. NOTE: Upstream comments indicate "this is being tre |
|            |                         |                  |                  |            ated as a non-security bug and no real threat."             |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |  Low CVE-2019-1010024   |     libc-bin     | 2.31-13+deb11u2  | ** DISPUTED ** GNU Libc current is affected by: Mitigation bypass. The |
|            |                         |                  |                  |  impact is: Attacker may bypass ASLR using cache of thread stack and h |
|            |                         |                  |                  | eap. The component is: glibc. NOTE: Upstream comments indicate "this i |
|            |                         |                  |                  |       s being treated as a non-security bug and no real threat."       |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |  Low CVE-2019-1010025   |     libc-bin     | 2.31-13+deb11u2  | ** DISPUTED ** GNU Libc current is affected by: Mitigation bypass. The |
|            |                         |                  |                  |  impact is: Attacker may guess the heap addresses of pthread_created t |
|            |                         |                  |                  | hread. The component is: glibc. NOTE: the vendor's position is "ASLR b |
|            |                         |                  |                  |                 ypass itself is not a vulnerability."                  |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2019-9192    |     libc-bin     | 2.31-13+deb11u2  | ** DISPUTED ** In the GNU C Library (aka glibc or libc6) through 2.29, |
|            |                         |                  |                  |  check_dst_limits_calc_pos_1 in posix/regexec.c has Uncontrolled Recur |
|            |                         |                  |                  | sion, as demonstrated by '(|)(\\1\\1)*' in grep, a different issue tha |
|            |                         |                  |                  | n CVE-2018-20796. NOTE: the software maintainer disputes that this is  |
|            |                         |                  |                  | a vulnerability because the behavior occurs only with a crafted patter |
|            |                         |                  |                  |                                   n.                                   |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2021-43396    |     libc-bin     | 2.31-13+deb11u2  | ** DISPUTED ** In iconvdata/iso-2022-jp-3.c in the GNU C Library (aka  |
|            |                         |                  |                  | glibc) 2.34, remote attackers can force iconv() to emit a spurious '\0 |
|            |                         |                  |                  | ' character via crafted ISO-2022-JP-3 data that is accompanied by an i |
|            |                         |                  |                  | nternal state reset. This may affect data integrity in certain iconv() |
|            |                         |                  |                  |  use cases. NOTE: the vendor states "the bug cannot be invoked through |
|            |                         |                  |                  |  user input and requires iconv to be invoked with a NULL inbuf, which  |
|            |                         |                  |                  | ought to require a separate application bug to do so unintentionally.  |
|            |                         |                  |                  |             Hence there's no security impact to the bug."              |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved | Critical CVE-2021-33574 |      libc6       | 2.31-13+deb11u2  | The mq_notify function in the GNU C Library (aka glibc) versions 2.32  |
|            |                         |                  |                  | and 2.33 has a use-after-free. It may use the notification thread attr |
|            |                         |                  |                  | ibutes object (passed through its struct sigevent parameter) after it  |
|            |                         |                  |                  | has been freed by the caller, leading to a denial of service (applicat |
|            |                         |                  |                  |            ion crash) or possibly unspecified other impact.            |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2010-4756    |      libc6       | 2.31-13+deb11u2  | The glob implementation in the GNU C Library (aka glibc or libc6) allo |
|            |                         |                  |                  | ws remote authenticated users to cause a denial of service (CPU and me |
|            |                         |                  |                  | mory consumption) via crafted glob expressions that do not match any p |
|            |                         |                  |                  | athnames, as demonstrated by glob expressions in STAT commands to an F |
|            |                         |                  |                  |        TP daemon, a different vulnerability than CVE-2010-2632.        |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2018-20796    |      libc6       | 2.31-13+deb11u2  | In the GNU C Library (aka glibc or libc6) through 2.29, check_dst_limi |
|            |                         |                  |                  | ts_calc_pos_1 in posix/regexec.c has Uncontrolled Recursion, as demons |
|            |                         |                  |                  |            trated by '(\227|)(\\1\\1|t1|\\\2537)+' in grep.            |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |  Low CVE-2019-1010022   |      libc6       | 2.31-13+deb11u2  | ** DISPUTED ** GNU Libc current is affected by: Mitigation bypass. The |
|            |                         |                  |                  |  impact is: Attacker may bypass stack guard protection. The component  |
|            |                         |                  |                  | is: nptl. The attack vector is: Exploit stack buffer overflow vulnerab |
|            |                         |                  |                  | ility and use this bypass vulnerability to bypass stack guard. NOTE: U |
|            |                         |                  |                  | pstream comments indicate "this is being treated as a non-security bug |
|            |                         |                  |                  |                          and no real threat."                          |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |  Low CVE-2019-1010023   |      libc6       | 2.31-13+deb11u2  | ** DISPUTED ** GNU Libc current is affected by: Re-mapping current loa |
|            |                         |                  |                  | ded library with malicious ELF file. The impact is: In worst case atta |
|            |                         |                  |                  | cker may evaluate privileges. The component is: libld. The attack vect |
|            |                         |                  |                  | or is: Attacker sends 2 ELF files to victim and asks to run ldd on it. |
|            |                         |                  |                  |  ldd execute code. NOTE: Upstream comments indicate "this is being tre |
|            |                         |                  |                  |            ated as a non-security bug and no real threat."             |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |  Low CVE-2019-1010024   |      libc6       | 2.31-13+deb11u2  | ** DISPUTED ** GNU Libc current is affected by: Mitigation bypass. The |
|            |                         |                  |                  |  impact is: Attacker may bypass ASLR using cache of thread stack and h |
|            |                         |                  |                  | eap. The component is: glibc. NOTE: Upstream comments indicate "this i |
|            |                         |                  |                  |       s being treated as a non-security bug and no real threat."       |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |  Low CVE-2019-1010025   |      libc6       | 2.31-13+deb11u2  | ** DISPUTED ** GNU Libc current is affected by: Mitigation bypass. The |
|            |                         |                  |                  |  impact is: Attacker may guess the heap addresses of pthread_created t |
|            |                         |                  |                  | hread. The component is: glibc. NOTE: the vendor's position is "ASLR b |
|            |                         |                  |                  |                 ypass itself is not a vulnerability."                  |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2019-9192    |      libc6       | 2.31-13+deb11u2  | ** DISPUTED ** In the GNU C Library (aka glibc or libc6) through 2.29, |
|            |                         |                  |                  |  check_dst_limits_calc_pos_1 in posix/regexec.c has Uncontrolled Recur |
|            |                         |                  |                  | sion, as demonstrated by '(|)(\\1\\1)*' in grep, a different issue tha |
|            |                         |                  |                  | n CVE-2018-20796. NOTE: the software maintainer disputes that this is  |
|            |                         |                  |                  | a vulnerability because the behavior occurs only with a crafted patter |
|            |                         |                  |                  |                                   n.                                   |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2021-43396    |      libc6       | 2.31-13+deb11u2  | ** DISPUTED ** In iconvdata/iso-2022-jp-3.c in the GNU C Library (aka  |
|            |                         |                  |                  | glibc) 2.34, remote attackers can force iconv() to emit a spurious '\0 |
|            |                         |                  |                  | ' character via crafted ISO-2022-JP-3 data that is accompanied by an i |
|            |                         |                  |                  | nternal state reset. This may affect data integrity in certain iconv() |
|            |                         |                  |                  |  use cases. NOTE: the vendor states "the bug cannot be invoked through |
|            |                         |                  |                  |  user input and requires iconv to be invoked with a NULL inbuf, which  |
|            |                         |                  |                  | ought to require a separate application bug to do so unintentionally.  |
|            |                         |                  |                  |             Hence there's no security impact to the bug."              |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2013-0340    |    libexpat1     |     2.2.10-2     | expat 2.1.0 and earlier does not properly handle entities expansion un |
|            |                         |                  |                  | less an application developer uses the XML_SetEntityDeclHandler functi |
|            |                         |                  |                  | on, which allows remote attackers to cause a denial of service (resour |
|            |                         |                  |                  | ce consumption), send HTTP requests to intranet servers, or read arbit |
|            |                         |                  |                  | rary files via a crafted XML document, aka an XML External Entity (XXE |
|            |                         |                  |                  | ) issue.  NOTE: it could be argued that because expat already provides |
|            |                         |                  |                  |  the ability to disable external entity expansion, the responsibility  |
|            |                         |                  |                  | for resolving this issue lies with application developers; according t |
|            |                         |                  |                  | o this argument, this entry should be REJECTed, and each affected appl |
|            |                         |                  |                  |                    ication would need its own CVE.                     |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   High CVE-2021-33560   |   libgcrypt20    |     1.8.7-6      | Libgcrypt before 1.8.8 and 1.9.x before 1.9.3 mishandles ElGamal encry |
|            |                         |                  |                  | ption because it lacks exponent blinding to address a side-channel att |
|            |                         |                  |                  | ack against mpi_powm, and the window size is not chosen appropriately. |
|            |                         |                  |                  |          This, for example, affects use of ElGamal in OpenPGP.         |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2018-6829    |   libgcrypt20    |     1.8.7-6      | cipher/elgamal.c in Libgcrypt through 1.8.2, when used to encrypt mess |
|            |                         |                  |                  | ages directly, improperly encodes plaintexts, which allows attackers t |
|            |                         |                  |                  | o obtain sensitive information by reading ciphertext data (i.e., it do |
|            |                         |                  |                  | es not have semantic security in face of a ciphertext-only attack). Th |
|            |                         |                  |                  | e Decisional Diffie-Hellman (DDH) assumption does not hold for Libgcry |
|            |                         |                  |                  |                      pt's ElGamal implementation.                      |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   High CVE-2021-43618   |     libgmp10     |  2:6.2.1+dfsg-1  | GNU Multiple Precision Arithmetic Library (GMP) through 6.2.1 has an m |
|            |                         |                  |                  | pz/inp_raw.c integer overflow and resultant buffer overflow via crafte |
|            |                         |                  |                  |     d input, leading to a segmentation fault on 32-bit platforms.      |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2011-3389    |   libgnutls30    |     3.7.1-5      | The SSL protocol, as used in certain configurations in Microsoft Windo |
|            |                         |                  |                  | ws and Microsoft Internet Explorer, Mozilla Firefox, Google Chrome, Op |
|            |                         |                  |                  | era, and other products, encrypts data by using CBC mode with chained  |
|            |                         |                  |                  | initialization vectors, which allows man-in-the-middle attackers to ob |
|            |                         |                  |                  | tain plaintext HTTP headers via a blockwise chosen-boundary attack (BC |
|            |                         |                  |                  | BA) on an HTTPS session, in conjunction with JavaScript code that uses |
|            |                         |                  |                  |  (1) the HTML5 WebSocket API, (2) the Java URLConnection API, or (3) t |
|            |                         |                  |                  |          he Silverlight WebClient API, aka a "BEAST" attack.           |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2004-0971    | libgssapi-krb5-2 | 1.18.3-6+deb11u1 | The krb5-send-pr script in the kerberos5 (krb5) package in Trustix Sec |
|            |                         |                  |                  | ure Linux 1.5 through 2.1, and possibly other operating systems, allow |
|            |                         |                  |                  | s local users to overwrite files via a symlink attack on temporary fil |
|            |                         |                  |                  |                                  es.                                   |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2018-5709    | libgssapi-krb5-2 | 1.18.3-6+deb11u1 | An issue was discovered in MIT Kerberos 5 (aka krb5) through 1.16. The |
|            |                         |                  |                  | re is a variable "dbentry->n_key_data" in kadmin/dbutil/dump.c that ca |
|            |                         |                  |                  | n store 16-bit data but unknowingly the developer has assigned a "u4"  |
|            |                         |                  |                  | variable to it, which is for 32-bit data. An attacker can use this vul |
|            |                         |                  |                  | nerability to affect other artifacts of the database as we know that a |
|            |                         |                  |                  |           Kerberos database dump file contains trusted data.           |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2004-0971    |   libk5crypto3   | 1.18.3-6+deb11u1 | The krb5-send-pr script in the kerberos5 (krb5) package in Trustix Sec |
|            |                         |                  |                  | ure Linux 1.5 through 2.1, and possibly other operating systems, allow |
|            |                         |                  |                  | s local users to overwrite files via a symlink attack on temporary fil |
|            |                         |                  |                  |                                  es.                                   |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2018-5709    |   libk5crypto3   | 1.18.3-6+deb11u1 | An issue was discovered in MIT Kerberos 5 (aka krb5) through 1.16. The |
|            |                         |                  |                  | re is a variable "dbentry->n_key_data" in kadmin/dbutil/dump.c that ca |
|            |                         |                  |                  | n store 16-bit data but unknowingly the developer has assigned a "u4"  |
|            |                         |                  |                  | variable to it, which is for 32-bit data. An attacker can use this vul |
|            |                         |                  |                  | nerability to affect other artifacts of the database as we know that a |
|            |                         |                  |                  |           Kerberos database dump file contains trusted data.           |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2004-0971    |    libkrb5-3     | 1.18.3-6+deb11u1 | The krb5-send-pr script in the kerberos5 (krb5) package in Trustix Sec |
|            |                         |                  |                  | ure Linux 1.5 through 2.1, and possibly other operating systems, allow |
|            |                         |                  |                  | s local users to overwrite files via a symlink attack on temporary fil |
|            |                         |                  |                  |                                  es.                                   |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2018-5709    |    libkrb5-3     | 1.18.3-6+deb11u1 | An issue was discovered in MIT Kerberos 5 (aka krb5) through 1.16. The |
|            |                         |                  |                  | re is a variable "dbentry->n_key_data" in kadmin/dbutil/dump.c that ca |
|            |                         |                  |                  | n store 16-bit data but unknowingly the developer has assigned a "u4"  |
|            |                         |                  |                  | variable to it, which is for 32-bit data. An attacker can use this vul |
|            |                         |                  |                  | nerability to affect other artifacts of the database as we know that a |
|            |                         |                  |                  |           Kerberos database dump file contains trusted data.           |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2004-0971    | libkrb5support0  | 1.18.3-6+deb11u1 | The krb5-send-pr script in the kerberos5 (krb5) package in Trustix Sec |
|            |                         |                  |                  | ure Linux 1.5 through 2.1, and possibly other operating systems, allow |
|            |                         |                  |                  | s local users to overwrite files via a symlink attack on temporary fil |
|            |                         |                  |                  |                                  es.                                   |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2018-5709    | libkrb5support0  | 1.18.3-6+deb11u1 | An issue was discovered in MIT Kerberos 5 (aka krb5) through 1.16. The |
|            |                         |                  |                  | re is a variable "dbentry->n_key_data" in kadmin/dbutil/dump.c that ca |
|            |                         |                  |                  | n store 16-bit data but unknowingly the developer has assigned a "u4"  |
|            |                         |                  |                  | variable to it, which is for 32-bit data. An attacker can use this vul |
|            |                         |                  |                  | nerability to affect other artifacts of the database as we know that a |
|            |                         |                  |                  |           Kerberos database dump file contains trusted data.           |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2021-39537    |   libncursesw6   |  6.2+20201114-2  | An issue was discovered in ncurses through v6.2-1. _nc_captoinfo in ca |
|            |                         |                  |                  |              ptoinfo.c has a heap-based buffer overflow.               |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2017-11164    |     libpcre3     |    2:8.39-13     | In PCRE 8.41, the OP_KETRMAX feature in the match function in pcre_exe |
|            |                         |                  |                  | c.c allows stack exhaustion (uncontrolled recursion) when processing a |
|            |                         |                  |                  |                       crafted regular expression.                      |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2017-16231    |     libpcre3     |    2:8.39-13     | ** DISPUTED ** In PCRE 8.41, after compiling, a pcretest load test PoC |
|            |                         |                  |                  |  produces a crash overflow in the function match() in pcre_exec.c beca |
|            |                         |                  |                  | use of a self-recursive call. NOTE: third parties dispute the relevanc |
|            |                         |                  |                  | e of this report, noting that there are options that can be used to li |
|            |                         |                  |                  |                 mit the amount of stack that is used.                  |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2017-7245    |     libpcre3     |    2:8.39-13     | Stack-based buffer overflow in the pcre32_copy_substring function in p |
|            |                         |                  |                  | cre_get.c in libpcre1 in PCRE 8.40 allows remote attackers to cause a  |
|            |                         |                  |                  | denial of service (WRITE of size 4) or possibly have unspecified other |
|            |                         |                  |                  |                       impact via a crafted file.                       |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2017-7246    |     libpcre3     |    2:8.39-13     | Stack-based buffer overflow in the pcre32_copy_substring function in p |
|            |                         |                  |                  | cre_get.c in libpcre1 in PCRE 8.40 allows remote attackers to cause a  |
|            |                         |                  |                  | denial of service (WRITE of size 268) or possibly have unspecified oth |
|            |                         |                  |                  |                     er impact via a crafted file.                      |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2019-20838    |     libpcre3     |    2:8.39-13     | libpcre in PCRE before 8.43 allows a subject buffer over-read in JIT w |
|            |                         |                  |                  | hen UTF is disabled, and \X or \R has more than one fixed quantifier,  |
|            |                         |                  |                  |                   a related issue to CVE-2019-20454.                   |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2021-36084    |    libsepol1     |      3.1-1       | The CIL compiler in SELinux 3.2 has a use-after-free in __cil_verify_c |
|            |                         |                  |                  | lassperms (called from __cil_verify_classpermission and __cil_pre_veri |
|            |                         |                  |                  |                              fy_helper).                               |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2021-36085    |    libsepol1     |      3.1-1       | The CIL compiler in SELinux 3.2 has a use-after-free in __cil_verify_c |
|            |                         |                  |                  | lassperms (called from __verify_map_perm_classperms and hashtab_map).  |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2021-36086    |    libsepol1     |      3.1-1       | The CIL compiler in SELinux 3.2 has a use-after-free in cil_reset_clas |
|            |                         |                  |                  | spermission (called from cil_reset_classperms_set and cil_reset_classp |
|            |                         |                  |                  |                              erms_list).                               |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2021-36087    |    libsepol1     |      3.1-1       | The CIL compiler in SELinux 3.2 has a heap-based buffer over-read in e |
|            |                         |                  |                  | bitmap_match_any (called indirectly from cil_check_neverallow). This o |
|            |                         |                  |                  | ccurs because there is sometimes a lack of checks for invalid statemen |
|            |                         |                  |                  |                        ts in an optional block.                        |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2021-36690    |   libsqlite3-0   |     3.34.1-3     | ** DISPUTED ** A segmentation fault can occur in the sqlite3.exe comma |
|            |                         |                  |                  | nd-line component of SQLite 3.36.0 via the idxGetTableInfo function wh |
|            |                         |                  |                  | en there is a crafted SQL query. NOTE: the vendor disputes the relevan |
|            |                         |                  |                  | ce of this report because a sqlite3.exe user already has full privileg |
|            |                         |                  |                  | es (e.g., is intentionally allowed to execute commands). This report d |
|            |                         |                  |                  |            oes NOT imply any problem in the SQLite library.            |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2007-6755    |    libssl1.1     | 1.1.1k-1+deb11u1 | The NIST SP 800-90A default statement of the Dual Elliptic Curve Deter |
|            |                         |                  |                  | ministic Random Bit Generation (Dual_EC_DRBG) algorithm contains point |
|            |                         |                  |                  |  Q constants with a possible relationship to certain "skeleton key" va |
|            |                         |                  |                  | lues, which might allow context-dependent attackers to defeat cryptogr |
|            |                         |                  |                  | aphic protection mechanisms by leveraging knowledge of those values.   |
|            |                         |                  |                  | NOTE: this is a preliminary CVE for Dual_EC_DRBG; future research may  |
|            |                         |                  |                  | provide additional details about point Q and associated attacks, and c |
|            |                         |                  |                  |        ould potentially lead to a RECAST or REJECT of this CVE.        |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2010-0928    |    libssl1.1     | 1.1.1k-1+deb11u1 | OpenSSL 0.9.8i on the Gaisler Research LEON3 SoC on the Xilinx Virtex- |
|            |                         |                  |                  | II Pro FPGA uses a Fixed Width Exponentiation (FWE) algorithm for cert |
|            |                         |                  |                  | ain signature calculations, and does not verify the signature before p |
|            |                         |                  |                  | roviding it to a caller, which makes it easier for physically proximat |
|            |                         |                  |                  | e attackers to determine the private key via a modified supply voltage |
|            |                         |                  |                  |       for the microprocessor, related to a "fault-based attack."       |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2013-4392    |   libsystemd0    |     247.3-6      | systemd, when updating file permissions, allows local users to change  |
|            |                         |                  |                  | the permissions and SELinux security contexts for arbitrary files via  |
|            |                         |                  |                  |                 a symlink attack on unspecified files.                 |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2020-13529    |   libsystemd0    |     247.3-6      | An exploitable denial-of-service vulnerability exists in Systemd 245.  |
|            |                         |                  |                  | A specially crafted DHCP FORCERENEW packet can cause a server running  |
|            |                         |                  |                  | the DHCP client to be vulnerable to a DHCP ACK spoofing attack. An att |
|            |                         |                  |                  | acker can forge a pair of FORCERENEW and DCHP ACK packets to reconfigu |
|            |                         |                  |                  |                             re the server.                             |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2021-39537    |    libtinfo6     |  6.2+20201114-2  | An issue was discovered in ncurses through v6.2-1. _nc_captoinfo in ca |
|            |                         |                  |                  |              ptoinfo.c has a heap-based buffer overflow.               |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2013-4392    |     libudev1     |     247.3-6      | systemd, when updating file permissions, allows local users to change  |
|            |                         |                  |                  | the permissions and SELinux security contexts for arbitrary files via  |
|            |                         |                  |                  |                 a symlink attack on unspecified files.                 |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2020-13529    |     libudev1     |     247.3-6      | An exploitable denial-of-service vulnerability exists in Systemd 245.  |
|            |                         |                  |                  | A specially crafted DHCP FORCERENEW packet can cause a server running  |
|            |                         |                  |                  | the DHCP client to be vulnerable to a DHCP ACK spoofing attack. An att |
|            |                         |                  |                  | acker can forge a pair of FORCERENEW and DCHP ACK packets to reconfigu |
|            |                         |                  |                  |                             re the server.                             |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2007-5686    |      login       |    1:4.8.1-1     | initscripts in rPath Linux 1 sets insecure permissions for the /var/lo |
|            |                         |                  |                  | g/btmp file, which allows local users to obtain sensitive information  |
|            |                         |                  |                  | regarding authentication attempts.  NOTE: because sshd detects the ins |
|            |                         |                  |                  | ecure permissions and does not log certain events, this also prevents  |
|            |                         |                  |                  | sshd from logging failed authentication attempts by remote attackers.  |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2013-4235    |      login       |    1:4.8.1-1     | shadow: TOCTOU (time-of-check time-of-use) race condition when copying |
|            |                         |                  |                  |                      and removing directory trees                      |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2019-19882    |      login       |    1:4.8.1-1     | shadow 4.8, in certain circumstances affecting at least Gentoo, Arch L |
|            |                         |                  |                  | inux, and Void Linux, allows local users to obtain root access because |
|            |                         |                  |                  |  setuid programs are misconfigured. Specifically, this affects shadow  |
|            |                         |                  |                  | 4.8 when compiled using --with-libpam but without explicitly passing - |
|            |                         |                  |                  | -disable-account-tools-setuid, and without a PAM configuration suitabl |
|            |                         |                  |                  | e for use with setuid account management tools. This combination leads |
|            |                         |                  |                  |  to account management tools (groupadd, groupdel, groupmod, useradd, u |
|            |                         |                  |                  | serdel, usermod) that can easily be used by unprivileged local users t |
|            |                         |                  |                  | o escalate privileges to root in multiple ways. This issue became much |
|            |                         |                  |                  |  more relevant in approximately December 2019 when an unrelated bug wa |
|            |                         |                  |                  | s fixed (i.e., the chmod calls to suidusbins were fixed in the upstrea |
|            |                         |                  |                  |     m Makefile which is now included in the release version 4.8).      |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2021-39537    |   ncurses-base   |  6.2+20201114-2  | An issue was discovered in ncurses through v6.2-1. _nc_captoinfo in ca |
|            |                         |                  |                  |              ptoinfo.c has a heap-based buffer overflow.               |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2021-39537    |   ncurses-bin    |  6.2+20201114-2  | An issue was discovered in ncurses through v6.2-1. _nc_captoinfo in ca |
|            |                         |                  |                  |              ptoinfo.c has a heap-based buffer overflow.               |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2007-6755    |     openssl      | 1.1.1k-1+deb11u1 | The NIST SP 800-90A default statement of the Dual Elliptic Curve Deter |
|            |                         |                  |                  | ministic Random Bit Generation (Dual_EC_DRBG) algorithm contains point |
|            |                         |                  |                  |  Q constants with a possible relationship to certain "skeleton key" va |
|            |                         |                  |                  | lues, which might allow context-dependent attackers to defeat cryptogr |
|            |                         |                  |                  | aphic protection mechanisms by leveraging knowledge of those values.   |
|            |                         |                  |                  | NOTE: this is a preliminary CVE for Dual_EC_DRBG; future research may  |
|            |                         |                  |                  | provide additional details about point Q and associated attacks, and c |
|            |                         |                  |                  |        ould potentially lead to a RECAST or REJECT of this CVE.        |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2010-0928    |     openssl      | 1.1.1k-1+deb11u1 | OpenSSL 0.9.8i on the Gaisler Research LEON3 SoC on the Xilinx Virtex- |
|            |                         |                  |                  | II Pro FPGA uses a Fixed Width Exponentiation (FWE) algorithm for cert |
|            |                         |                  |                  | ain signature calculations, and does not verify the signature before p |
|            |                         |                  |                  | roviding it to a caller, which makes it easier for physically proximat |
|            |                         |                  |                  | e attackers to determine the private key via a modified supply voltage |
|            |                         |                  |                  |       for the microprocessor, related to a "fault-based attack."       |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2007-5686    |      passwd      |    1:4.8.1-1     | initscripts in rPath Linux 1 sets insecure permissions for the /var/lo |
|            |                         |                  |                  | g/btmp file, which allows local users to obtain sensitive information  |
|            |                         |                  |                  | regarding authentication attempts.  NOTE: because sshd detects the ins |
|            |                         |                  |                  | ecure permissions and does not log certain events, this also prevents  |
|            |                         |                  |                  | sshd from logging failed authentication attempts by remote attackers.  |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2013-4235    |      passwd      |    1:4.8.1-1     | shadow: TOCTOU (time-of-check time-of-use) race condition when copying |
|            |                         |                  |                  |                      and removing directory trees                      |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |   Low CVE-2019-19882    |      passwd      |    1:4.8.1-1     | shadow 4.8, in certain circumstances affecting at least Gentoo, Arch L |
|            |                         |                  |                  | inux, and Void Linux, allows local users to obtain root access because |
|            |                         |                  |                  |  setuid programs are misconfigured. Specifically, this affects shadow  |
|            |                         |                  |                  | 4.8 when compiled using --with-libpam but without explicitly passing - |
|            |                         |                  |                  | -disable-account-tools-setuid, and without a PAM configuration suitabl |
|            |                         |                  |                  | e for use with setuid account management tools. This combination leads |
|            |                         |                  |                  |  to account management tools (groupadd, groupdel, groupmod, useradd, u |
|            |                         |                  |                  | serdel, usermod) that can easily be used by unprivileged local users t |
|            |                         |                  |                  | o escalate privileges to root in multiple ways. This issue became much |
|            |                         |                  |                  |  more relevant in approximately December 2019 when an unrelated bug wa |
|            |                         |                  |                  | s fixed (i.e., the chmod calls to suidusbins were fixed in the upstrea |
|            |                         |                  |                  |     m Makefile which is now included in the release version 4.8).      |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |  Medium CVE-2020-16156  |    perl-base     | 5.32.1-4+deb11u2 |                    [Signature Verification Bypass]                     |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2011-4116    |    perl-base     | 5.32.1-4+deb11u2 | _is_safe in the File::Temp module for Perl does not properly handle sy |
|            |                         |                  |                  |                                mlinks.                                 |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
| Unapproved |    Low CVE-2005-2541    |       tar        |   1.34+dfsg-1    | Tar 1.15.1 does not properly warn the user when extracting setuid or s |
|            |                         |                  |                  | etgid files, which may allow local users or remote attackers to gain p |
|            |                         |                  |                  |                               rivileges.                               |
+------------+-------------------------+------------------+------------------+------------------------------------------------------------------------+
```
